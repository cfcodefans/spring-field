package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.ClassEn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassEnRepo extends CrudRepository<ClassEn, Long> {
    ClassEn findByName(String name);
}
