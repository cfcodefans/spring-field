package cf.study.spring.data.reflects.entity;

import com.google.gson.Gson;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.lang.model.element.Modifier;
import javax.persistence.*;
import java.util.*;

import static java.lang.reflect.Modifier.*;

@Entity
@Table(name = "base_en", indexes = {@Index(name = "name_idx", columnList = "name"), @Index(columnList = "category", name = "cat_idx")})
@Inheritance(strategy = InheritanceType.JOINED)
@Cacheable(false)
//@DiscriminatorColumn(name="category", discriminatorType = DiscriminatorType.STRING)
public class BaseEn extends AbstractPersistable<Long> {

    @Basic
    @Column(name = "name", length = 512)
    public String name;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "enclosing", nullable = true)
    public BaseEn enclosing;

    //	@Transient
    @OneToMany(cascade = {CascadeType.REFRESH}, mappedBy = "enclosing")
    public Collection<BaseEn> children = Collections.synchronizedCollection(new LinkedHashSet<BaseEn>());

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    public CategoryEn category = CategoryEn.DEFAULT;

    @ManyToMany(cascade = {CascadeType.REFRESH})
    @JoinTable(name = "annotations",
            joinColumns = {@JoinColumn(name = "base_en_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "annotation_en_id", referencedColumnName = "id")})
    public List<ClassEn> annotations = new LinkedList<ClassEn>();

    @Version
    public long version;

    public BaseEn(String qualifiedName, BaseEn enclosing, CategoryEn cat) {
        this();
        this.name = qualifiedName;
        this.enclosing = enclosing;
        this.category = cat;

        if (enclosing != null)
            enclosing.children.add(this);
    }

    public BaseEn() {

    }

    public static Set<Modifier> getModifiers(int mod) {
        Set<Modifier> ms = new LinkedHashSet<Modifier>();

        if (isAbstract(mod))
            ms.add(Modifier.ABSTRACT);
        if (isFinal(mod))
            ms.add(Modifier.FINAL);
        if (isNative(mod))
            ms.add(Modifier.NATIVE);
        if (isPrivate(mod))
            ms.add(Modifier.PRIVATE);
        if (isProtected(mod))
            ms.add(Modifier.PROTECTED);
        if (isPublic(mod))
            ms.add(Modifier.PUBLIC);
        if (isStatic(mod))
            ms.add(Modifier.STATIC);
        if (isStrict(mod))
            ms.add(Modifier.STRICTFP);
        if (isSynchronized(mod))
            ms.add(Modifier.SYNCHRONIZED);
        if (isTransient(mod))
            ms.add(Modifier.TRANSIENT);
        if (isVolatile(mod))
            ms.add(Modifier.VOLATILE);

        return ms;
    }

    @Override
    public String toString() {
//		return Jsons.toString(this);
        return category + ": " + name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        if (!isNew()) return result;

        return Objects.hash(result, category, enclosing, name, version);
    }

    @Override
    public boolean equals(Object obj) {
        BaseEn other = (BaseEn) obj;

        if (!isNew() && super.equals(obj)) return true;

        return Objects.equals(category, other.category)
                && Objects.equals(enclosing, other.enclosing)
                && Objects.equals(name, other.name)
                && Objects.equals(version, other.version);
    }

    public BaseEn clone() {
        return clone(null);
    }

    public BaseEn clone(BaseEn _be) {
        if (_be == null) {
            _be = new BaseEn();
        }

        _be.setId(getId());
        _be.name = name;
        _be.category = category;
        _be.version = version;

        Collection<BaseEn> children2 = _be.children;
        children.stream().map(this::clone).forEach(children2::add);

        return _be;
    }

    public static final Gson GSON = new Gson();

}
