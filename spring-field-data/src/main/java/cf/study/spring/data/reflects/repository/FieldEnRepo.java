package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.FieldEn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FieldEnRepo extends CrudRepository<FieldEn, Long> {
}
