package cf.study.spring.data.reflects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataLoader {
    public static final Logger log = LogManager.getLogger(DataLoader.class);

    @Test
    public void testMisc() {
        String[] classPaths = StringUtils.split(SystemUtils.JAVA_CLASS_PATH, ';');

        log.info(IntStream.range(0, classPaths.length).mapToObj(i -> i + "\t" + classPaths[i]).collect(Collectors.joining("\n\t")));
    }

    @Test
    public void testWithRuntime() throws Exception {
        File _f = new File(String.format("%s/lib/rt.jar", SystemUtils.JAVA_HOME));
        List<Class<?>> re = Reflects.loadClzzFromJar(_f, ClassLoader.getSystemClassLoader());
        System.out.println(StringUtils.join(re, '\n'));
        System.out.println(re.size());
    }

    @Test
    public void testRun() {
        String[] classPaths = Arrays.stream(StringUtils.split(SystemUtils.JAVA_CLASS_PATH, ';'))
            .filter(p -> !StringUtils.containsIgnoreCase(p, "idea"))
            .filter(p -> !StringUtils.containsIgnoreCase(p, "ext"))
            .filter(p -> (StringUtils.containsIgnoreCase(p, "jre")) ? StringUtils.containsIgnoreCase(p, "rt") : true)
            .toArray(String[]::new);

        log.info(IntStream.range(0, classPaths.length).mapToObj(i -> i + "\t" + classPaths[i]).collect(Collectors.joining("\n\t")));

        DataCollector dc = new DataCollector();
        for (String path : classPaths) {
            dc.loadFromJar(new File((path)));
        }
    }
}
