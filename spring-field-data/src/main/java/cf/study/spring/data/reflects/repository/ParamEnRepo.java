package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.ParameterEn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParamEnRepo extends CrudRepository<ParameterEn, Long> {
}
