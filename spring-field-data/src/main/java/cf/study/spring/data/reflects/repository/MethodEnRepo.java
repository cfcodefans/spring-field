package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.FieldEn;
import cf.study.spring.data.reflects.entity.MethodEn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MethodEnRepo extends CrudRepository<MethodEn, Long> {
}
