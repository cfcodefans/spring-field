package cf.study.spring.data.reflects.batch;

import cf.study.spring.data.reflects.Reflects;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.support.IteratorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BatchProcTests.BatchCfgs.class})
public class BatchProcTests {

    static Logger log = LogManager.getLogger(BatchProcTests.class);

    @Configuration
    @EnableBatchProcessing
    public static class BatchCfgs extends DefaultBatchConfigurer {
        @Autowired
        public JobBuilderFactory jbf;

        @Autowired
        public StepBuilderFactory sbf;

        @Bean(name = "source")
        @Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
        public List<Class<?>> clzz() {
            File _f = new File(String.format("%s/lib/rt.jar", SystemUtils.JAVA_HOME));
            List<Class<?>> clzList = Reflects.extractClazz(_f);
            return clzList;
        }

        @Autowired
        @Qualifier("source")
        public List<Class<?>> clzList;

        @Bean
        @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public ItemReader<Class<?>> reader() {
            return new IteratorItemReader<Class<?>>(clzList.iterator());
        }
    }


}
