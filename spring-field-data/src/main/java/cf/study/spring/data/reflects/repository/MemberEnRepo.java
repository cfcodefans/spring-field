package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.ClassEn;
import cf.study.spring.data.reflects.entity.MemberEn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberEnRepo extends CrudRepository<MemberEn, Long> {
}
