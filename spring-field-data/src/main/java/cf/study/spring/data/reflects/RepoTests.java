package cf.study.spring.data.reflects;

import cf.study.spring.data.reflects.entity.BaseEn;
import cf.study.spring.data.reflects.entity.ClassEn;
import cf.study.spring.data.reflects.repository.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RepoTests.RepoCfgs.class})
public class RepoTests {
    @Configuration
    @EnableJpaRepositories(entityManagerFactoryRef = "EMF", transactionManagerRef = "TM")
    @ComponentScan("cf.study.spring.data.reflects")
    static class RepoCfgs {
        @Bean(name = "EMF")
        public EntityManagerFactory getEMF() {
            return Persistence.createEntityManagerFactory("spring-field");
        }

        @Bean(name = "TM")
        public PlatformTransactionManager getTM() {
            return new JpaTransactionManager(getEMF());
        }
    }

    public static final Logger log = LogManager.getLogger(RepoTests.class);

    @Autowired
    EntityManager em;

    @Test
    public void testEntityManager() {
        log.info(em);
        Assert.assertNotNull(em);
    }

    @Test
    public void testJpa() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("spring-field");
        log.info(emf.getMetamodel());
        emf.close();
    }

    @Autowired
    ApplicationContext appCtx;

    @Test
    public void testAppCtx() {
        Assert.assertNotNull(appCtx);
        log.info(appCtx);
        log.info(StringUtils.join(appCtx.getBeanDefinitionNames(), "\n"));
    }

    @Autowired
    ClassEnRepo clzRepo;

    @Autowired
    BaseEnRepo baseRepo;

    @Autowired
    MemberEnRepo memberEnRepo;

    @Autowired
    FieldEnRepo fieldEnRepo;

    @Autowired
    MethodEnRepo methodEnRepo;

    @Autowired
    ParamEnRepo paramEnRepo;

    @Test
    public void loadData() {
        log.info(clzRepo);
        Assert.assertNotNull(clzRepo);

        Iterable<ClassEn> allClzz = clzRepo.findAll();
        log.info(allClzz);
    }

    @Test
    public void testLoad() {
        DataCollector dc = new DataCollector();
        File _f = new File(String.format("%s/lib/rt.jar", SystemUtils.JAVA_HOME));
        dc.loadFromJar(_f);
    }

    @Autowired
    private PlatformTransactionManager tm;

    @Test
    public void testSave() {
        DataCollector dc = new DataCollector();
        File _f = new File(String.format("%s/lib/rt.jar", SystemUtils.JAVA_HOME));
        dc.loadFromJar(_f);

        Iterable<BaseEn> saveds = baseRepo.saveAll(dc.queue);

        List<String> sqlList = StreamSupport.stream(saveds.spliterator(), false)
            .map(DataCollector::associateByNativeSql)
            .flatMap(List::stream)
            .collect(Collectors.toList());

        log.info("sql list: \t" + sqlList.size());

        TransactionTemplate temp = new TransactionTemplate(tm);

        temp.execute((TransactionStatus ts) -> {
            for (String sql : sqlList) {
                em.createNativeQuery(sql).executeUpdate();
            }
            em.flush();
            return null;
        });
    }

    @Test
    public void verifySave() {
        log.info("\n\n\n");
        log.info("count: \t" + baseRepo.count());
        log.info("class count: \t" + clzRepo.count());
        log.info("member count: \t" + memberEnRepo.count());
        baseRepo.groupByCategory().stream().map(row -> StringUtils.join(row, "\t")).forEach(log::info);

        BaseEn byName = baseRepo.findByName(String.class.getName());

        log.info("\n\n\n");
    }

    @Test
    public void testFindMethods() {
        BaseEn byName = baseRepo.findByName(String.class.getName());
        log.info(byName);
        ClassEn byName1 = clzRepo.findByName(String.class.getName());
        log.info(byName1);
    }

}
