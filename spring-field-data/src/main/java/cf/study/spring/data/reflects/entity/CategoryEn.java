package cf.study.spring.data.reflects.entity;

public enum CategoryEn {
	PACKAGE,
	CLASS,
	FIELD,
	METHOD,
	CONSTRUCTOR,
	DEFAULT,
	MEMBER,
	PARAMETER
}
