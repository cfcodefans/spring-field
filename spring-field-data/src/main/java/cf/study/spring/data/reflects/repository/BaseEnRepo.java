package cf.study.spring.data.reflects.repository;

import cf.study.spring.data.reflects.entity.BaseEn;
import cf.study.spring.data.reflects.entity.ClassEn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BaseEnRepo extends CrudRepository<BaseEn, Long> {
    @Query("select be.category, count(be.category) as cnt from BaseEn be group by category")
    List<Object[]> groupByCategory();

    BaseEn findByName(String name);
}
