package cf.study.spring.boot.web

import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.ImportResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
//@ImportResource("classpath:app-config.xml")
object App {
    private val log = LoggerFactory.getLogger(App::class.java)

    var appCtx: ConfigurableApplicationContext? = null
        private set

    @JvmStatic
    fun main(args: Array<String>) {
        appCtx = SpringApplication.run(App::class.java, *args)
    }

    @RestController
    @RequestMapping("/stub")
    open class StubCtrl {
        @GetMapping("/hello/{name}")
        open fun get(name: String): String = "Hello, $name"
    }
}
