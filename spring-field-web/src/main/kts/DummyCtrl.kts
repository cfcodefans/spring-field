import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.AbstractController

class DummyCtrl : AbstractController() {

    @Throws(Exception::class)
    override fun handleRequestInternal(req: HttpServletRequest,
                                       resp: HttpServletResponse): ModelAndView {
        val model = ModelAndView("hello")
        model.addObject("message", "Hello World!")
        return model
    }
}

DummyCtrl::class.java