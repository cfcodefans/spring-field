import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.annotation.PostConstruct

@RestController
@RequestMapping("/test")
open class TestCtrl {


    //    companion object {
    private val log = LoggerFactory.getLogger(TestCtrl::class.java)
//    }

    init {
        log.debug("TestCtrl init block")
    }

    @PostConstruct
    open fun postConstruct(): Unit {
        log.debug("TestCtrl.postConstruct $appCtx")
    }

    @Autowired
    lateinit var appCtx: ApplicationContext

    @GetMapping(path = ["/echo/{req}"])
    @ResponseStatus(HttpStatus.OK)
    open fun echo(@PathVariable("req") reqStr: String): String = "${Date()}\n\t$reqStr"
}
//TestCtrl()
TestCtrl::class.java