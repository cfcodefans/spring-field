package cf.study.spring.tests;

import cf.study.spring.cfg.java.JavaCfgTests;
import cf.study.spring.dummy.services.IService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static cf.study.spring.cfg.java.JavaCfgTests.JavaCfg.SERVICE_UPPER;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {JavaCfgTests.JavaCfg.class})
public class SpringJUnitTests {
    static Logger log = LogManager.getLogger(SpringJUnitTests.class);

    @Component
    public static class BeanPostLogger implements BeanPostProcessor {
        static Logger log = LogManager.getLogger(BeanPostLogger.class);
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if (!bean.getClass().getName().startsWith("cf")) return bean;
            log.info("\nBeanPostLogger.postProcessBeforeInitialization(\n\t{},\n\t{})\n", bean, beanName);
            return bean;
        }
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (!bean.getClass().getName().startsWith("cf")) return bean;
            log.info("\nBeanPostLogger.postProcessAfterInitialization(\n\t{},\n\t{})\n", bean, beanName);
            return bean;
        }
    }

    @Component
    public static class AutowiredAnnoBeanPostLogger extends AutowiredAnnotationBeanPostProcessor {

    }

    @Autowired @Qualifier(SERVICE_UPPER)
    public IService<String, String> upper;

    @Test
    public void testUpper() {
        log.info(upper.serve("abc"));
    }
}
