package cf.study.spring.cfg.java;

import cf.study.spring.dummy.services.IService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {JavaCfgTests.JavaCfg.class})
public class JavaCfgTests {

    static Logger log = LogManager.getLogger(JavaCfgTests.class);
    @Autowired ApplicationContext ctx;

    @Configuration
    @ComponentScan(basePackages = "cf.study.spring")
    public static class JavaCfg {
        public static final String SERVICE_UPPER = "service:upper";
        public static final String SERVICE_UPPER1 = "service:_upper";

        @Bean(name = SERVICE_UPPER, initMethod = "init")
        public IService<String, String> getUpperService() {
            return (String str) -> str.toUpperCase();
        }

        @Bean(name = SERVICE_UPPER1, initMethod = "init")
        @Scope("prototype")
        public IService<String, String> getNewUpperService() {
            return (String str) -> str.toUpperCase();
        }
    }

    @Test
    public void testAnnoCfgAppCtx() {
        IService bean = ctx.getBean("service:upper", IService.class);
        log.info(bean.serve("hey, hey"));
        log.info(bean.serve("hey, hey"));

        IService _bean = ctx.getBean("service:_upper", IService.class);
        log.info(_bean.serve("hey, hey"));
        log.info(_bean.serve("hey, hey"));

        IService component = ctx.getBean(IService.DummyComponent.class);
        log.info(component.serve("hey, hey"));
        log.info(component.serve("hey, hey"));
    }

    @Test
    public void testPrototypeScope() {
        log.info(ctx.getBean("service:upper", IService.class).serve("hey, hey"));
        log.info(ctx.getBean("service:upper", IService.class).serve("hey, hey"));

        log.info(ctx.getBean("service:_upper", IService.class).serve("hey, hey"));
        log.info(ctx.getBean("service:_upper", IService.class).serve("hey, hey"));
    }

}
