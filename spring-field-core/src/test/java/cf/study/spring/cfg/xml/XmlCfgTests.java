package cf.study.spring.cfg.xml;

import cf.study.spring.cfg.java.JavaCfgTests;
import cf.study.spring.dummy.services.IService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration("context-tests.xml")
public class XmlCfgTests {
    static Logger log = LogManager.getLogger(XmlCfgTests.class);

    @Autowired
    ApplicationContext ctx;

    @Test
    public void beanConstructor() {
        Object strBean = ctx.getBean("bean:str");
        log.info(strBean);

        Object _strBean = ctx.getBean("bean:_str");
        log.info(_strBean);

        Assert.assertEquals(strBean, _strBean);
        Assert.assertFalse(strBean == _strBean);
    }

    @Test
    public void factoryMethod() {
        log.info(ctx.getBean("bean:factory:cal"));
    }

    @Test
    public void initMethod() {
        log.info(ctx.getBean("dummy:singleton", IService.class).serve("who"));
        log.info(ctx.getBean("dummy:singleton", IService.class).serve("who"));
    }

    @Test
    public void scope() {
        log.info(ctx.getBean("dummy:singleton", IService.class).serve("who"));
        log.info(ctx.getBean("dummy:singleton", IService.class).serve("who"));

        log.info(ctx.getBean("dummy:new", IService.class).serve("who"));
        log.info(ctx.getBean("dummy:new", IService.class).serve("who"));
    }
}
