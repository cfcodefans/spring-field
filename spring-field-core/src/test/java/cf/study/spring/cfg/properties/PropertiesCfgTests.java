package cf.study.spring.cfg.properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class PropertiesCfgTests {

    @Component
    @Configuration
    @PropertySource("test.properties")
    @ConfigurationProperties(prefix = "test")
    public static class PropertyCfgs {
        int number;
        String string;
        List<String> array;

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }

        public List<String> getArray() {
            return array;
        }

        public void setArray(List<String> array) {
            this.array = array;
        }
    }

    @Autowired
    PropertyCfgs propertyCfgs;

    @Test
    public void test() {
        Assert.assertNotNull(propertyCfgs);
        System.out.println(ToStringBuilder.reflectionToString(propertyCfgs, ToStringStyle.JSON_STYLE));
    }
}
