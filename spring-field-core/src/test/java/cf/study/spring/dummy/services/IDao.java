package cf.study.spring.dummy.services;

import cf.study.misc.MiscUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IDao {
    List<? extends Object> query(String sql);

    @Repository
    class DummyDao implements IDao {
        @Override
        public List<? extends Object> query(String sql) {
            return MiscUtils.pi2Longs(Integer.parseInt(sql));
        }
    }
}
