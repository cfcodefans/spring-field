package cf.study.spring.dummy.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

public interface IService<T, R> {
    Logger log = LogManager.getLogger(IService.class);

    R serve(T t);

    @PostConstruct
    default void init() {
        log.info("IService.init()");
    }

    class Dummy implements IService<Object, String> {
        @Override
        public String serve(Object s) {
            return s + " is dummy";
        }
    }

    @Component
    class DummyComponent implements IService<Object, String> {
        @PostConstruct
        public void init() {
            log.info("DummyComponent.init()");
        }
        @Override
        public String serve(Object s) {
            return s + " is dummy Component";
        }

        @Autowired
        private IDao dao;
    }

    static IService<Object, String> getDummy() {
        log.info("IService.getDummy()");
        return new Dummy();
    }
}
