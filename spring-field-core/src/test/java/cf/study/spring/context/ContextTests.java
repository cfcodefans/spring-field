package cf.study.spring.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.function.Supplier;

@RunWith(SpringRunner.class)
@ContextConfiguration("context-tests.xml")
public class ContextTests {
    static Logger log = LogManager.getLogger(ContextTests.class);
    @Autowired
    ApplicationContext ctx;

    @Test
    public void testCtx() {
        log.info(ctx);
        Assert.assertNotNull(ctx);
    }

    @Autowired
    @Qualifier("script-runner1")
    Runnable ktRunner;

    @Autowired
    @Qualifier("script-supplier1")
    Supplier<String> ktSupplier;

    @Test
    public void testScriptRunner() {
        log.info(ktRunner);
        Assert.assertNotNull(ktRunner);
        Class<? extends Runnable> aClass = ktRunner.getClass();
        log.info("ktRunner is {}", aClass);
        Arrays.stream(aClass.getDeclaredMethods())
            .forEach(log::info);
        ktRunner.run();
    }

    @Test
    public void testScriptSupplier() {
        log.info(ktSupplier);
        Assert.assertNotNull(ktSupplier);
        Class<? extends Supplier> aClass = ktSupplier.getClass();
        log.info("ktSupplier is {}", aClass);
        Arrays.stream(aClass.getDeclaredMethods())
            .forEach(log::info);
        ktSupplier.get();
    }
}
