package cf.study.spring.context;

import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmDaemonLocalEvalScriptEngineFactory;
import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptTests {
    @Test
    public void testKotlinScriptEngine() throws ScriptException {
        KotlinJsr223JvmDaemonLocalEvalScriptEngineFactory sef = new KotlinJsr223JvmDaemonLocalEvalScriptEngineFactory();
        ScriptEngineManager sem = new ScriptEngineManager();
        sem.registerEngineExtension(sef.getExtensions().get(0), sef);
        sem.getEngineFactories().stream().forEach(System.out::println);
        ScriptEngine engine = sem.getEngineByExtension("kts");
        System.out.println(engine);
//        System.out.println(sef.getEngineName());
        Object re = engine.eval("class KtRunnable:Runnable {override fun run():Unit {print(\"hi\")}}; KtRunnable()");
        System.out.println(re);
        Runnable r = (Runnable)re;
        ((Runnable) re).run();
    }
}
