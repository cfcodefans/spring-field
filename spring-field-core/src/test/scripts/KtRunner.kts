package cf.study.spring.context

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
class KtRunner : Runnable {
    @Autowired
    var appCtx: ApplicationContext? = null

    @Autowired
    @Qualifier("dummy")
    var dummy: String? = null

    override fun run() {
        println(this)
        println(appCtx)
        println(this.dummy)
    }
}

KtRunner()