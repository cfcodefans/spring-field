package cf.study.spring.context

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.util.function.Supplier

@Service
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
class KtSupplier() : Supplier<String> {

//    constructor() : this("null value") {
//    }

    @Autowired
    @Qualifier("dummy")
    lateinit var injected: String

    @Autowired
    var appCtx: ApplicationContext? = null

    lateinit var value: String
//        get() = value
//        set(value) {
//            this.value = value
//        }

    override fun get(): String {
        println(this.injected)
        println(this.value)
        return value!!
    }
}

KtSupplier::class.java