package cf.study.spring.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static java.lang.String.format;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {EventTests.EventCfgs.class})
public class EventTests {
    static Logger log = LogManager.getLogger(EventTests.class);

    @Autowired
    ConfigurableApplicationContext appCtx;

    @Test
    public void foo() {
        appCtx.start();
        log.info("EventTests.foo");
        appCtx.stop();
    }

    @Configuration
    @ComponentScan(basePackages = "cf.study.spring.events")
    public static class EventCfgs {
        @Bean
        public ApplicationListener<ContextRefreshedEvent> getRefreshEventHandler() {
            return (ContextRefreshedEvent event) -> log.info("\n\tEventCfgs.getRefreshEventHandler(\n\t\t{})", event);
        }
    }

    @Component
    public static class AppListener implements ApplicationListener {
        @Override
        public void onApplicationEvent(ApplicationEvent event) {
            log.info("\n\tAppListener.onApplicationEvent(\n\t\t{})", event);
        }
    }

    public static class TestEvent extends ApplicationEvent {
        public final String msg;

        /**
         * Create a new ApplicationEvent.
         *
         * @param source the object on which the event initially occurred (never {@code null})
         */
        public TestEvent(Object source, String msg) {
            super(source);
            this.msg = msg;
        }

        @Override
        public String toString() {
            return format("%s:{source:\t%s,\tmsg:\t%s,\ttimestampe:\t{}}", TestEvent.class.getSimpleName(), super.source, this.msg, this.getTimestamp());
        }
    }

    @Component
    public static class TestEventSrc {
        @Autowired
        private ApplicationEventPublisher appEventPublisher;

        public void fireEvent(final String msg) {
            log.info("\n\t{}\n\tEventSrc.fireEvent(\n\t\t{})", Thread.currentThread(), msg);
            appEventPublisher.publishEvent(new TestEvent(this, msg));
        }

        @Autowired
        ApplicationEventMulticaster appEventMulticaster;

        public void multicastEvent(final String msg) {
            log.info("\n\t{}\n\tEventSrc.multicastEvent(\n\t\t{})", Thread.currentThread(), msg);
            appEventMulticaster.multicastEvent(new TestEvent(this, msg));
        }

        @Autowired
        SimpleApplicationEventMulticaster asynEventMulticaster;

        @PostConstruct
        private void setUp() {
//            asynEventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
            asynEventMulticaster.setTaskExecutor(new ThreadPoolTaskExecutor());
        }

        public void asyncMulticastEvent(final String msg) {
            log.info("\n\t{}\n\tEventSrc.asyncMulticastEvent(\n\t\t{})", Thread.currentThread(), msg);
            asynEventMulticaster.multicastEvent(new TestEvent(this, msg));
        }
    }

    @Autowired
    TestEventSrc tes;

    @Component
    public static class TestEventListener1 implements ApplicationListener<TestEvent> {
        @Override
        public void onApplicationEvent(TestEvent event) {
            log.info("\n\t{}\n\tTestEventListener1.onApplicationEvent(\n\t\t{})", Thread.currentThread(), event);
        }
    }

    @Component
    public static class TestEventListener2 implements ApplicationListener<TestEvent> {
        @Override
        public void onApplicationEvent(TestEvent event) {
            log.info("\n\t{}\n\tTestEventListener2.onApplicationEvent(\n\t\t{})", Thread.currentThread(), event);
        }
    }

    @Test
    public void testCustomEvent() {
        tes.fireEvent("This is a TestEvent");
    }

    @Test
    public void testMulticast() {
        tes.multicastEvent("This is a TestEvent");
    }

    @Test
    public void testAsyncMulticast() {
        tes.asyncMulticastEvent("This is a TestEvent");
    }
}
